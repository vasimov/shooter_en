// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ShooterEnGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTEREN_API AShooterEnGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
